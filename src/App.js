import { Component } from "react";
import { CharacterList } from "./components/CharacterList ";

class App extends Component {
  state = {
    character: [],
  };
  componentDidMount() {
    fetch("https://rickandmortyapi.com/api/character/").then((response) =>
      response
        .json()
        .then((response) => this.setState({ character: response.results }))
    );
  }
  render() {
    return <CharacterList list={this.state.character} />;
  }
}
export default App;
